module GF3HelperTest {

    function testGetHourUI() {
        var result = GF3Helper.getHourUI(0.0, true);
        GF3Assert.isEqual(result[0], 0, "Expected 24: 0 -> 0");
        GF3Assert.isEqual(result[1], "", "Expected 0");

        result = GF3Helper.getHourUI(0.0, false);
        GF3Assert.isEqual(result[0], 12, "Expected 12: 0 -> 12");
        GF3Assert.isEqual(result[1], "AM", "Expected 12 AM");

        result = GF3Helper.getHourUI(6.0, true);
        GF3Assert.isEqual(result[0], 6, "Expected 24: 6 -> 6");
        GF3Assert.isEqual(result[1], "", "Expected 6");

        result = GF3Helper.getHourUI(6.0, false);
        GF3Assert.isEqual(result[0], 6, "Expected 12: 6 -> 6");
        GF3Assert.isEqual(result[1], "AM", "Expected 6 AM");

        result = GF3Helper.getHourUI(15.0, true);
        GF3Assert.isEqual(result[0], 15, "Expected 12: 15 -> 15");
        GF3Assert.isEqual(result[1], "", "Expected 15");

        result = GF3Helper.getHourUI(15.0, false);
        GF3Assert.isEqual(result[0], 3, "Expected 12: 15 -> 3");
        GF3Assert.isEqual(result[1], "PM", "Expected 15 PM");
    }
}