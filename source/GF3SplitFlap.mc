using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Time.Gregorian as Cal;
using Toybox.ActivityMonitor as Act;

class GF3SplitFlap extends Ui.WatchFace {
    var cx = 109;  // Fenix3 only (for the time being)
    var cy = 109;
    var roadIcon;
    var stepsIcon;
    var burnedIcon;
    var blueToothIcon;
    var is24Hour = false;

    //! Constructor
    function initialize() {
        roadIcon = Ui.loadResource(Rez.Drawables.road);
        stepsIcon = Ui.loadResource(Rez.Drawables.steps);
        burnedIcon = Ui.loadResource(Rez.Drawables.burned);
        blueToothIcon = Ui.loadResource(Rez.Drawables.bluetooth);
        is24Hour = Sys.getDeviceSettings().is24Hour;
    }

    function onUpdate(dc) {
        // unit test, remove before production !!
        // GF3Test.test();
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_BLACK, Gfx.COLOR_BLACK);
        dc.clear();
        dc.setColor(Gfx.COLOR_DK_RED, Gfx.COLOR_DK_RED, Gfx.COLOR_DK_RED);
        dc.fillRoundedRectangle(7, 62, 204, 94, 3);

        var now = Time.now();
        var info = Cal.info(now, Time.FORMAT_LONG);
        var hour= Sys.getClockTime().hour;
        var min = Sys.getClockTime().min;
        var hourUI = GF3Helper.getHourUI(hour, is24Hour);

        GF3Graphics.drawTopPanel(dc, cx, cy, roadIcon, burnedIcon, blueToothIcon);
        GF3Graphics.drawBottomPanel(dc, cx, cy, stepsIcon);

        var panel2 = cy+16;
        var panel3 = cy-14;
        if(is24Hour) {
            panel2 = cy-14;
            panel3 = cy+16;
        }

        GF3Graphics.drawSplitFlap(dc, cx-98, cy-44 , 60, 28, {:text  => info.day_of_week,        :color => Gfx.COLOR_WHITE, :font => Gfx.FONT_MEDIUM,          :yOffset => -2});
        GF3Graphics.drawSplitFlap(dc, cx-98, panel2, 60, 28, {:text  => info.day.format("%02d"), :color => Gfx.COLOR_WHITE, :font => Gfx.FONT_MEDIUM,          :yOffset => -2});
        GF3Graphics.drawSplitFlap(dc, cx-98, panel3, 60, 28, {:text  => info.month,              :color => Gfx.COLOR_WHITE, :font => Gfx.FONT_MEDIUM,          :yOffset => -2});
        GF3Graphics.drawSplitFlap(dc, cx-34, cy-44 , 64, 88, {:text  => hourUI[0].format("%0d"), :color => Gfx.COLOR_WHITE, :font => Gfx.FONT_NUMBER_THAI_HOT, :yOffset => -5});
        GF3Graphics.drawSplitFlap(dc, cx+34, cy-44 , 64, 88, {:text  => min.format("%02d"),      :color => Gfx.COLOR_WHITE, :font => Gfx.FONT_NUMBER_THAI_HOT, :yOffset => -5});

        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
        if(hourUI[1].equals("AM")) {
            dc.drawText(cx-29, cy-38, Gfx.FONT_XTINY, "A", Gfx.TEXT_JUSTIFY_LEFT);
            dc.drawText(cx-29, cy-25, Gfx.FONT_XTINY, "M", Gfx.TEXT_JUSTIFY_LEFT);
        } else if(hourUI[1].equals("PM")) {
            dc.drawText(cx-29, cy+3 , Gfx.FONT_XTINY, "P", Gfx.TEXT_JUSTIFY_LEFT);
            dc.drawText(cx-29, cy+16, Gfx.FONT_XTINY, "M", Gfx.TEXT_JUSTIFY_LEFT);
        }
    }
}