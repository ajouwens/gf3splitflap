using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.ActivityMonitor as Act;

module GF3Graphics {

    function drawTopPanel(dc, cx, cy, roadIcon, burnedIcon, blueToothIcon){
        var calories = (Act.getInfo().calories).toDouble();
        var battPerc = (Sys.getSystemStats().battery).toDouble();
        if (Sys.getDeviceSettings().phoneConnected == true) {
            dc.drawBitmap(cx-6, cy-102, blueToothIcon);
        }
        drawBattery(dc, cx, cy, battPerc);
        drawDistance(dc, cx+6, cy-70, roadIcon);
        drawCalories(dc, cx+81, cy-70, calories, burnedIcon);
    }

    function drawBottomPanel(dc, cx, cy, icon){
        var stepGoal = (Act.getInfo().stepGoal).toDouble();
        var steps = (Act.getInfo().steps).toDouble();
        var moveBar = (Act.getInfo().moveBarLevel).toDouble();
        var height = 60;
        var relPos = 100.0;

        // Steps & Step Goal
        if(stepGoal != null && stepGoal > 0.0){
            relPos = steps <=  stepGoal ? (steps / stepGoal) * 100.0 : 100.0;
            drawGauge(dc, cx-50, cy+55, 100, 14, icon, steps.format("%0d"), relPos, Gfx.COLOR_DK_BLUE);
        }
        // Move Bar Level
        drawGauge(dc, cx-50, cy+75, 100, 14, null, "", 100.0, getMoveBarColor(moveBar));
    }

    function drawBattery(dc, cx, cy, battPerc) {
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(cx-46, cy-78,  2, 6);
        dc.drawRectangle(cx-70, cy-80, 24, 10);
        var width = 21.0 * (battPerc/100) + 1;
        if(battPerc < 25) {
            dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
        } else if (battPerc < 50) {
            dc.setColor(Gfx.COLOR_YELLOW, Gfx.COLOR_TRANSPARENT);
        } else {
            dc.setColor(Gfx.COLOR_GREEN, Gfx.COLOR_TRANSPARENT);
        }
        dc.fillRectangle(cx-69, cy-79, width, 8);
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
        dc.drawText(cx-70, cy-72, Gfx.FONT_XTINY, battPerc.format("%0d") + "%", Gfx.TEXT_JUSTIFY_LEFT);
    }

    function drawCalories(dc, posX, posY, calories, burnedIcon) {
        dc.drawText(posX, posY, Gfx.FONT_XTINY, calories.format("%0d") , Gfx.TEXT_JUSTIFY_RIGHT);
        dc.drawBitmap(posX-28, posY-16, burnedIcon);
    }

    function drawDistance(dc, posX, posY, roadIcon) {
        var text;
        var distance = Act.getInfo().distance.toFloat();
        if(Sys.getDeviceSettings().distanceUnits == 1) {
            text = (distance / 160934).format( "%.02f") + "mi";
        } else {
            text = (distance / 100000).format( "%.02f") + "km";
        }
        dc.drawText(posX, posY, Gfx.FONT_XTINY, text , Gfx.TEXT_JUSTIFY_CENTER);
        dc.drawBitmap(posX-18, posY-14, roadIcon);
    }

    function drawGauge(dc, posX, posY, width, height, icon, text, level, color){
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        dc.drawRoundedRectangle(posX, posY, width, height, 3);
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
        dc.fillRoundedRectangle(posX+1, posY+1, width-2, height-2, 3);
        dc.setColor(Gfx.COLOR_DK_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.drawRoundedRectangle(posX+1, posY+1, width-2, height-2, 3);
        dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.drawRoundedRectangle(posX+2, posY+2, width-4, height-4, 3);
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
        if(icon != null) {
            dc.drawBitmap(posX-21, posY, icon);
        }
        dc.drawText(posX + width + 2, posY+5, Gfx.FONT_XTINY, text, Gfx.TEXT_JUSTIFY_LEFT | Gfx.TEXT_JUSTIFY_VCENTER);
        var actLevel = (width-6) * (level/100);
        dc.setColor(color, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(posX+3, posY+3, actLevel, height-6);
    }

    function getMoveBarColor(moveBar) {
        if(moveBar >= 4.0) {
            return Gfx.COLOR_RED;
        } else if(moveBar >= 3.0) {
            return Gfx.COLOR_ORANGE;
        } else if(moveBar >= 2.0) {
            return Gfx.COLOR_YELLOW;
        } else if(moveBar >= 1.0) {
            return Gfx.COLOR_BLUE;
        }
        return Gfx.COLOR_GREEN;
    }

    function drawSplitFlap(dc, posX, posY, width, height, text) {
        var middleX  = posX+(width/2);
        var middleY  = posY+(height/2);
        var rightSide = posX+width;

        // Draw the 'main' panel
        drawMainPanel(dc, posX, posY, width, height);
        // Draw the text
        drawPanelText(dc, middleX, middleY + text.get(:yOffset), text);
        // Draw the 3D-effect
        draw3Deffect(dc, posX, posY, rightSide, middleY);
        // Draw the 'wheels'
        drawWheels(dc, posX, middleY, rightSide);
    }

    function drawMainPanel(dc, posX, posY, width, height) {
        dc.setColor(Gfx.COLOR_DK_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.fillRoundedRectangle(posX, posY, width, height, 3);
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        dc.fillRoundedRectangle(posX+1, posY+2, width-2, height-2, 3);
        dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.drawLine(posX+1, posY+1, posX+width-2, posY+1);
    }

    function drawPanelText(dc, posX, posY, text) {
        dc.setColor(text.get(:color), Gfx.COLOR_TRANSPARENT);
        dc.drawText(posX, posY, text.get(:font), text.get(:text), Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
    }

    function draw3Deffect(dc, posX, posY, rightSide, middleY) {
        dc.setColor(Gfx.COLOR_DK_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.drawLine(posX+2, middleY-1, rightSide-2, middleY-1);
        dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.drawLine(posX+2, middleY, rightSide-2, middleY);
    }

    function drawWheels(dc, posX, middleY, rightSide) {
        // Left wheel
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(posX+1, middleY-6, 3, 12);
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(posX+1, middleY-5, 2, 3);
        dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(posX+1, middleY-2, 2, 3);
        dc.setColor(Gfx.COLOR_DK_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(posX+1, middleY+1, 2, 4);
        // Right wheel
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(rightSide-4, middleY-6, 3, 12);
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(rightSide-3, middleY-5, 2, 3);
        dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(rightSide-3, middleY-2, 2, 3);
        dc.setColor(Gfx.COLOR_DK_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(rightSide-3, middleY+1, 2, 4);
    }
}