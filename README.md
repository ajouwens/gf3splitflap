GF3SplitFlap - Digital watchface
===============================================

Description
-----------
This is digital watch face with a split-flap design
-----------
Please, leave your comments, bug reports and ideas on this forum:
https://forums.garmin.com/showthread.php?265723-Watchfaces-GF3-
-
Version history
---------------
V1.0:
+ Day / month
+ Time (12/24)
+ Battery status icon + percentage
+ Today's distance (mi/km)
+ Today's calories burned (kcal)
+ Today's step count
+ Move bar (green => blue => yellow => orange => red)
